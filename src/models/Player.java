package models;

import java.util.List;

public class Player {
    private String name;
    private String playerPawn;
    private List<String> xPlayed;
    private List<String> yPlayed;

    public Player(String name,String playerPawn) {
        this.name = name;
        this.playerPawn = playerPawn;
    }

    public String getName() {
        return name;
    }

    public String getPlayerPawn() {
        return playerPawn;
    }

    public List<String> getxPlayed() {
        return xPlayed;
    }

    public void setxPlayed(List<String> xPlayed) {
        this.xPlayed = xPlayed;
    }

    public List<String> getyPlayed() {
        return yPlayed;
    }

    public void setyPlayed(List<String> yPlayed) {
        this.yPlayed = yPlayed;
    }
}
