import models.Line;

import java.util.*;

public class TicTacToe {

        public static void main(String[] args) {

            List<String> list = new ArrayList<>();
            list.add("1");
            list.add("2");
            list.add("3");
            int winSequence = 0;

            Line line1 = new Line(" _______"+System.lineSeparator()+"| - - - |");
            Line line2 = new Line("| - - - |");
            Line line3 = new Line("| - - - |"+System.lineSeparator()+" _______");

            List<Line> Lines= List.of(line1,line2,line3);


            System.out.println(line1.getGridContent()+System.lineSeparator()+line2.getGridContent()+System.lineSeparator()+line3.getGridContent());
            Set<String> distinct = new HashSet<>(list);
            for (String s: distinct) {
                System.out.println(s + ": " + Collections.frequency(list, s));
                if(Collections.frequency(list, s) == 3){
                    System.out.println("win");
                    break;
                }else{
                    winSequence++;
                    if(winSequence == 3){
                        System.out.println("win");
                        break;
                    }
                }
            }
        }
    }